# _*_ encoding=utf-8 _*_
# @time:2022/2/9 
# Author:赵王发
# @file:dir_config.py
# @software:PyCharm
# 获取文件路径

import os
import datetime

time = datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')

# 项目根目录
path = os.path.dirname(os.path.dirname(__file__))
# print(path)

# 测试用例数据的路径
test_data_path = os.path.join(path, 'datas', 'test_case.xlsx')
# print(test_data_path)

# 测试报告路径
test_report_path = os.path.join(path, 'results', 'reports', time, '.html')
# print(test_report_path)

# allure测试报告路径
allure_report_path = os.path.join(path, 'results', 'allure_reports')
# print(test_report_path)

# 日志路径
log_path = os.path.join(path, 'results', 'logs', '{0}.txt'.format(time))
# print(log_path)
