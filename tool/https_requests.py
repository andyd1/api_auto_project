# _*_ encoding=utf-8 _*_
# @time:2022/2/9 
# Author:赵王发
# @file:http_requests.py
# @software:PyCharm
# 封装requests请求
import requests
from tool.logger import Logger


class HttpsRequests:

    def https_requests(self, url, data, http_method, cookie=None):
        try:
            if http_method.upper() == 'GET':
                res = requests.get(url, params=data, cookies=cookie)
                return res  # 返回结果
            elif http_method.upper() == 'POST':
                res = requests.post(url, data, cookies=cookie)
                return res  # 返回结果
            else:
                Logger().info('输入的请求不对')

        except Exception as e:
            Logger().error(e)
            # print('get请求报错了{0}'.format(e))
            raise e  # 抛出异常
