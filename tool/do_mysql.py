# _*_ encoding=utf-8 _*_
# @time:2022/2/9 
# Author:赵王发
# @file:do_mysql.py
# @software:PyCharm
import mysql.connector
from config import config_data as cd


class DoMysql:

    @staticmethod
    def do_mysql(state='all'):
        # 创建一个数据库连接
        db_config = cd.db_config
        cn = mysql.connector.connect(**db_config)

        # 游标
        cursor = cn.cursor()
        # sql语句
        query_sql = 'sql查询语句'
        # 执行
        cursor.execute(query_sql)
        # 获取结果 打印结果
        if state == 'all':
            res = cursor.fetchone()  # 元组形式输出数据，针对的是一条数据
        else:
            res = cursor.fetchall()  # 列表嵌套元组形式输出数据，获取多条数据时用这个
        print(res)

        # 关闭游标
        cursor.close()

        # 关闭连接
        cn.close()
        return res
