# _*_ encoding=utf-8 _*_
# @time:2022/2/9 
# Author:赵王发
# @file:do_excel.py
# @software:PyCharm
from openpyxl import load_workbook
from config import config_data as cd
from config import dir_config as dc


class DoExcel:

    # 读取Excel表单里的数据
    @staticmethod
    def get_data(file_name):
        wb = load_workbook(file_name)   # 加载Excel表格
        mode = cd.mode
        test_data = []
        for key in mode:
            sheet = wb[key]  # 读取Excel的那个表单
            # print(sheet.cell(2, 1).value)
            if mode[key] == 'all':
                # print(sheet.cell(2, 1).value)
                for i in range(8, sheet.max_row+1):  # 读取第二行的所有字段数据
                    row_data = {}
                    row_data['case_id'] = sheet.cell(i, 1).value
                    row_data['case_name'] = sheet.cell(i, 2).value
                    row_data['url'] = sheet.cell(i, 3).value
                    row_data['data'] = sheet.cell(i, 4).value
                    row_data['http_method'] = sheet.cell(i, 5).value
                    row_data['expected'] = sheet.cell(i, 6).value
                    row_data['sheet_name'] = key
                    test_data.append(row_data)
            else:
                for case_id in mode[key]:
                    row_data = {}
                    row_data['case_id'] = sheet.cell(case_id + 7, 1).value
                    row_data['case_name'] = sheet.cell(case_id + 7, 2).value
                    row_data['url'] = sheet.cell(case_id + 7, 3).value
                    row_data['data'] = sheet.cell(case_id + 7, 4).value
                    row_data['http_method'] = sheet.cell(case_id + 7, 5).value
                    row_data['expected'] = sheet.cell(case_id + 7, 6).value
                    row_data['sheet_name'] = key
                    test_data.append(row_data)

        # print(test_data)  # 获取Excel的单元格值
        return test_data

    # 把数据写入Excel表单里
    @staticmethod
    def write_back(file_name, sheet_name, i, value_1, value_2):
        wb = load_workbook(file_name)   # 加载Excel表格
        # mode = eval(ReadConfig.get_config(config_path, 'MODE', 'mode'))
        # for key in mode:
        sheet = wb[sheet_name]
        sheet.cell(i, 7).value = str(value_1)
        sheet.cell(i, 8).value = value_2
        wb.save(file_name)

if __name__ == '__main__':
    res = DoExcel().get_data(dc.test_data_path)
    print(res)