# _*_ encoding=utf-8 _*_
# @time:2022/2/9 
# Author:赵王发
# @file:conftest.py
# @software:PyCharm
# pytest的配置
import pytest
from tool.do_excel import DoExcel
from config import dir_config as dc


@pytest.fixture(scope='class')
def web_access():
    test_data = DoExcel().get_data(dc.test_data_path)
    yield test_data
    pass
