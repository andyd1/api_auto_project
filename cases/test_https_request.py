# @Time : 2021/12/5 9:42 
# @Author : zhaowangfa
# @File : test_https_request.py
# @Software: PyCharm
# 接口测试用例模版封装
from tool.https_requests import HttpsRequests
from tool.do_excel import DoExcel
from config.dir_config import *
import pytest
from tool.logger import Logger

cookie = None


@pytest.mark.smoke
@pytest.mark.usefixtures('web_access')
class TestHttpRequests:

    # @pytest.mark.smoke
    def test_api(self, web_access):
        for item in web_access:
            print(item)
            # global cookie
            # result = ''
            # print('正在进行测试的是{0}'.format(item['case_name']))
            # res = HttpsRequests().https_requests(item['url'], eval(item['data']),
            #                                      item['http_method'], cookie)
            # if res.cookies:
            #     cookie = res.cookies
            #
            # # 判断测试结果
            # try:
            #     self.assertEqual(str(item['expected']), res.json()['code'])
            #     assert str(item['expected']), res.json()['code']
            #     Logger().info('用例执行通过Pass')
            #     result = 'Pass'
            # except Exception as e:
            #     result = 'Fail'
            #     Logger().error('用例执行失败Failed，原因{0}'.format(e))
            #     raise e
            # finally:
            #     # 写入测试结果
            #     DoExcel().write_back(test_data_path, item['sheet_name'],
            #                          item['case_id'] + 7, res.json(), result)
            #     Logger().info('测试结果是{0}'.format(res.json()))
