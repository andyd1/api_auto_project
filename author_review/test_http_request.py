# @Time : 2021/12/5 9:42 
# @Author : zhaowangfa
# @File : test_https_request.py
# @Software: PyCharm
# 接口测试用例模版封装
from tool.https_requests import HttpsRequests
from tool.do_excel import DoExcel
from config.dir_config import *
import pytest
from tool.logger import Logger

cookie = None


@pytest.mark.demo
@pytest.mark.usefixtures('web_access')
class TestHttpRequests:

    # @pytest.mark.smoke
    def test_api(self, web_access):
        for item in web_access:
            print(item)

