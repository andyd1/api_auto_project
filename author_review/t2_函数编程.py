# _*_ encoding=utf-8 _*_
# @time:2022/1/15 
# Author:赵王发
# @file:t2_函数编程.py
# @software:PyCharm
"""
函数的概念：对某个特定的功能或代码块进行封装，在需要使用该功能的时候直接调用即可
    定义：
    def 函数名():
        被封装的功能或代码块-->函数体
    调用:
    函数名()
函数的参数：
    实参：有值参数
    形参：
        位置参数，按照位置一个个去申明变量
        默认值参数，已传值，默认值参数放在位置参数后面
        动态形参
            *args
            **args  关键字动态参数

    位置参数放前面，关键字参数放后面
    实参在执行的时候，必须保证形参有数
    顺序：位置 >*args >默认值 > **kwargs
    想要改默认参数，要用关键字去改
函数的返回值：return
    函数只要执行到了return，函数就会立即停止并返回内容，函数内的return后续代码不会执行
    2、如果写了return
        1、只写了return。后面不跟数据，此时接收到的依然是None
        2、return值，此时表示函数有一个返回值，外界能够收到一个数据
        3、return 值1,值2,值3....，此时函数有多个返回值，外界接收到的是元祖，并且该元祖内存放所有的返回值
python内置函数
"""

# def buy_cai():
#     print('1、打车')
#     print('2、去菜市场')
#     print('3、选择菜')
#     print('4、讨价还价')
#     print('5、回家')
#
#
# buy_cai()
#
#
# # 有对象，处理到什么程度
# def maren(ren):
#     print(ren)
#
#
# maren('战三')  # 在调用函数的时候，传的值，叫做实参
#
#
# # 用一个函数定义一个计算器，实现+ - * /四则运算
# def caster(a, opt, b):
#     if opt == '+':
#         print(a + b)
#     elif opt == '-':
#         print(a + b)
#     elif opt == '*':
#         print(a * b)
#     elif opt == '/':
#         print(a / b)
#     else:
#         print('不是计算操作')
#
#
# # 位置参数和默认参数
# def func1(a, b=2):
#     print(a + b)
#
#
# # 动态参数
# def func2(*args):  # 接收到的所有参数都会被处理成元祖
#     print(args)
#     return args
#
#
# re = func2(1, 2, 3)
# print(type(re))
#
#
# # 关键字参数 ,接收到的所有参数都会被处理成字典
# def func3(**kwargs):
#     print(kwargs)
#
#
# func3(k1='word', k2='kiki')


# def func4(*args, **kwargs):  # 没有限制的接收任何参数
#     pass


lis = [1, 2, 3, 3, 4, 4, 5, 56, 6, 78, 8, 9]
dic = {'key1': '2', 'key2': '4', 'key3': '6', 'key4': '5', }


def func5(*args):
    print(args)


func5(*lis)  # *在实参位置，是把列表打散成位置参数进行传递


def func6(**kwargs):
    print(kwargs)


func6(**dic)
