# _*_ encoding=utf-8 _*_
# @time:2022/1/16 
# Author:赵王发
# @file:t9_装饰器.py
# @software:PyCharm
"""
装饰器：
    本质上是一个闭包
    作用：在不改变原有函数调用情况下，给函数增加新的功能，就是在原函数前后新增功能
    def wrapper(func):
        def inner(*args,**kwargs):
            ret = func(*args,**kwargs)
            return ret
        return inner
    @wrapper
    def func():
        pass
    结论：
        如果目标函数被多个装饰器装饰着
        @wrapper1
        @wrapper2
        def func():
            pass
        执行顺序:
        wrapper1 wrapper2  目标函数  wrapper2  wrapper1
"""

# def guanjia(game):
#     def inner():
#         print('准备调外层函数变量')
#         game()
#         print('外层函数调用完毕')
#
#     return inner
#
#
# @guanjia  # 艾特guanjia函数，相当play_wangzhe=guanjia(play_wangzhe)
# def play_wangzhe():
#     print('开始玩王者荣耀迭代')
#
#
# # play_wangzhe = guanjia(play_wangzhe)
# play_wangzhe()
# def guanjia(game):
#     def inner(*args, **kwargs):
#         print('准备调外层函数变量')
#         res = game(*args, **kwargs)
#         print('外层函数调用完毕')
#         return res
#
#     return inner
#
#
# @guanjia  # 艾特guanjia函数，相当play_wangzhe=guanjia(play_wangzhe)
# def play_wangzhe(agar1,agar2):
#     print('开始玩王者荣耀迭代',agar1,agar2)
#     return '一把屠龙刀'
#
#
# @guanjia
# def play_ruyaru(agar1,agar2,agar3):
#     print('开始撸呀撸，牛逼',agar1,agar2,agar3)
#
#
# # play_wangzhe = guanjia(play_wangzhe)
# play_wangzhe('test','game')
# play_ruyaru('admin','12345','刘备')

# def wrapper1(fn):
#     def inner(*args, **kwargs):
#         print('被wrapper1开始装饰')  # 1
#         ret = fn()                 # 4
#         print('被wrapper1装饰完成')  # 6
#         return ret
#     return inner
#
#
# def wrapper2(fn):
#     def inner(*args, **kwargs):
#         print('被wrapper2开始装饰')  # 2
#         ret = fn(*args, **kwargs)   # 3
#         print('被wrapper2装饰完成')  # 7
#         return ret
#     return inner
#
#
# @wrapper1   # func = wrapper1(wrapper2:inner)
# @wrapper2   # func = wrapper2(func)   即 wrapper2：inner
# def func():                          # 5
#     print('我是被两个装饰器进行装饰的')
#
#
# func()


# 例子
login_flag = False


def login_verify(fn):
    def inner(*args, **kwargs):
        global login_flag
        if not login_flag:
            while True:
                username = input('>>>')
                passwd = input('>>>')
                if username == 'admin' and passwd == '123':
                    print('登陆成功')
                    login_flag = True
                    break
                else:
                    print('登陆失败')
        ret = fn(*args, **kwargs)
        return ret
    return inner


@login_verify
def add():
    print('开始新增学生信息')


@login_verify
def change():
    print('修改学生信息')


add()
change()
