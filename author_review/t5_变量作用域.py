# _*_ encoding=utf-8 _*_
# @time:2022/1/15 
# Author:赵王发
# @file:t5_变量作用域.py
# @software:PyCharm
a = 20  # 全局变量,可以被函数调用


def func():
    print(a)


def func1():
    b = 18  # 局部变量，不处理的情况下，只能被自己使用
    print(b)


def func2():
    b = 18  # 局部变量，不处理的情况下，只能被自己使用
    print(b)
    return b  # return 返回后，可以被外部调用


ret = func2()
print(ret)
