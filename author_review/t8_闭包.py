# _*_ encoding=utf-8 _*_
# @time:2022/1/15 
# Author:赵王发
# @file:t8_闭包.py
# @software:PyCharm
"""
闭包: 本质，内层函数对外层函数的局部变量的使用或者说内层函数调用外层函数的局部变量，此时内层函数被称为闭包函数
    1、可以让一个变量常驻与内存
    2、可以避免全局变量被修改
"""


def func():
    a = 10

    def inner():
        nonlocal a
        a += 1
        return a

    return inner


ret = func()
r1 = ret()
print(r1)

r2 = ret()
print(r2)
