# _*_ encoding=utf-8 _*_
# @time:2022/1/15 
# Author:赵王发
# @file:t2_函数最终总结.py
# @software:PyCharm
"""
递归
函数的嵌套
变量的作用域
闭包
装饰器
    def wrapper(func):
        def inner(*args,**kwargs):
            ret = func(*args,**kwargs)
            return ret
        return inner
    @wrapper
    def func():
        pass
迭代器
生成器
    yield

    g = (x for x in xx)
推导式
匿名函数
python内置函数有下划线"_"、sorted、filter、map
"""

# 递归:函数自己调自己，如果没有任何东西拦截的话，它默认就是一个死循环
# python默认是有递归深度限制的，默认的最大递归深度是1000
# def func():
#     func()

i = 0


def func1():
    global i
    print('word')
    # print(i)
    if i < 2:
        i = i + 1  # 要先执行拦截条件，再调自己递归
        func1()


func1()


b = 20  # 全局变量


def func2():
    print(b)


func2()
