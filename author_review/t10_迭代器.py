# _*_ encoding=utf-8 _*_
# @time:2022/1/16 
# Author:赵王发
# @file:t10_迭代器.py
# @software:PyCharm
"""
for 变量 in 可迭代:
    pass
iterable:可迭代的东西
    str、list、tuple、dict、set、open()
    可迭代的数据类型都会提供一个叫迭代器的东西，这个迭代器可以帮我们把数据类型中的所有数据逐一的拿到
获取迭代器的两种方案：
    1、iter() 内置函数可以直接拿到迭代器
    2、_iter_() 特殊方法

从迭代器拿到数据：
    1、next() 内置函数
    2、_next_() 特殊方法

for里面一定要是拿迭代器的，所以所有不可迭代的东西不能用for循环
for循环里面一定有_next_出现

总结：迭代器统一了不同数据类型的遍历工作
迭代器本身也是可以迭代的
迭代器本身的特性：
    1、只能向前不能反复
    2、特别节省内存
    3、惰性机制


"""
# it = iter('是什么类型呀')
# print(type(it))
# print(next(it))
# print(next(it))


# 迭代器本身也是可以迭代的
s ='你好，可以的的的放非常'
it1 = s.__iter__()
print(it1)
for item in it1:
    print(item)