# _*_ encoding=utf-8 _*_
# @time:2022/1/15 
# Author:赵王发
# @file:t6_函数的嵌套.py
# @software:PyCharm
"""
函数可以嵌套函数
    1、函数可以作为返回值进行返回
    2、函数可以作为参数进行互相传递
    3、函数可以当作变量进行赋值给另一个变量
    函数名实际上就是一个变量名，都表示一个内存地址
"""


def func1():
    a = 2

    def func2():  # 函数的嵌套，局部变量
        print('kkk')

    print(a)
    func2()  # 局部的东西，一般都是在局部自己访问使用的


func1()


def func3():
    def inner():
        print('内嵌函数')

    return inner  # 返回的是一个函数，此时我们把一个函数当成一个变量进行返回的


b1 = func3()  # b1是inner
b1()


def func4(fn):
    print(fn)
    fn()


def func5():
    print('函数作为变量传值')


func4(func5)
