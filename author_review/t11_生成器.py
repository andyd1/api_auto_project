# _*_ encoding=utf-8 _*_
# @time:2022/1/19 
# Author:赵王发
# @file:t11_生成器.py
# @software:PyCharm
"""
生成器：
 生成本身就是迭代器

 创建生成器的两种方案“
    1、生成器函数
    2、生成器表达式
 生成器函数
    生成器函数中有一个关键字yield，有yield的一定是生成器
    生成器函数执行的时候，得到的是生成器，并不会执行函数
yield：只要函数中出现了yield，它就是一个生成器函数
    作用：
        1、可以返回数据
        2、可以分段执行函数中的内容，通过__next__()可以执行到下一个yield位置
    优势：
        用好了，特别节省内存
"""


def func():
    print('生成器')
    yield 123  # yield也有返回的意思，但要执行next后才会返回数据


# ret = func()   # 就是生成器
# print(ret)  # <generator object func at 0x000002A9CC097BC8>
# print(ret.__next__())   # 生成器就是迭代器


# 可以分段执行函数
# def func1():
#     print('分段1')
#     yield '返回第一段'
#     print('分段2')
#     yield '返回第二段'
#
#
# ret = func1()
# print(ret.__next__())
# print(ret.__next__())

# 作用例子
# 去工厂定制1000件衣服，每次只取100件
def order():
    lis = []

    for i in range(1000):
        lis.append('衣服{0}'.format(i))
        if len(lis) == 50:
            yield lis
            lis = []


ret = order()
print(ret.__next__())
print(ret.__next__())
