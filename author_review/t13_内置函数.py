# _*_ encoding=utf-8 _*_
# @time:2022/1/22 
# Author:赵王发
# @file:t13_内置函数.py
# @software:PyCharm
"""
zip：合并多个可迭代的数据
sorted：排序
filter：筛选
map：处理数据计算
"""
# lis = ['护岛神兽', '放', '反反复复放放风', '过分', '地方的']
# def func(item):
#     return len(item)
# # ret = func(lis)
# # print(ret)
# s = sorted(lis, key=func)
# print(s)
# func = lambda x: len(x)
# res = func(lis)
# print(res)
# s = sorted(lis, key=func)
# print(s)
lis = [{'name': '护岛神兽', 'id': 23}, {'name': '护岛神兽', 'id': 43},
       {'name': '护岛神兽', 'id': 53}, {'name': '护岛神兽', 'id': 2}, ]

s = sorted(lis, key=lambda d: d['id'])
print(s)

# s1 = '张三丰'
# s2 = s1.startswith('张')
# lis1 = ['张三丰', '李三思', '张无忌', '周星驰', '刘大海']
# f = filter(lambda s: s.startswith('张'), lis1)
# print(list(f))

lis2 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
m = map(lambda x:x*x, lis2)
print(list(m))

