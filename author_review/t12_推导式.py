# _*_ encoding=utf-8 _*_
# @time:2022/1/19 
# Author:赵王发
# @file:t12_推导式.py
# @software:PyCharm
"""
推导式：
    简化代码
    语法：
        列表推导式：[要返回的结果 for循环 if判断]
        字典推导式：{key:value for循环 if判断}
        集合推导式：(要返回的结果 for循环 if判断)

"""
# lis = []
# for i in range(10):
#     lis.append(i)
# print(lis)
# 以上可以改成列表推导式
lis = [i for i in range(10)]
print(lis)

# 例如：请创建一个列表存放[1,3,5,7,9]
lis1 = [i for i in range(1, 10, 2)]
print(lis1)

# 生成50件衣服
# lis2 = [f'衣服{i}' for i in range(50)]
# print(lis2)

# 将如下列表中的所有英文字母修改成大写
lis3 = ['aller', 'admin', 'kiki', 'lily']
lis4 = [item.upper() for item in lis3]
print(lis4)

# 集合推导式
s = {i for i in range(10)}
print(s)

# 字典推导式
# 将下列的列表修改位字典，要求索引值为key，数据作为value
lis5 = ['赵本山','范围','王大拿','淡淡的']
dic = {f'{i}':lis5[i] for i in range(len(lis5))}
print(dic)
