# _*_ encoding=utf-8 _*_
# @time:2022/1/13 
# Author:赵王发
# @file:t1_文件操作.py
# @software:PyCharm
"""
open(文件路径,mode='',encond='')
文件路径：
    绝对路径
    相对路径

 mode:
    r 读取
    w 写入
    a 追加
    b 读写的是非文本文件 ->bytes
    with 上下文，不需要手动去关闭一个文件
"""
# f = open('t1_读取的文件.txt', mode='r', encoding='utf-8')
# # print(f.readline().strip())  # 一行一行输出
# # print(f.readline().strip())
#
# print(f.readline())
#
# print(f.readlines())  # 输出列表，后面带换号
# print(f.readable())  # 判断文件是否为可读，返回布尔值
# print(f.read(3))

# 读取图片
# with open('22.png', mode='rb') as f:
#     for line in f:
#         print(line)

# # 文件复制
# with open('1.png',mode='rb') as f1,open('2.png',mode='wb') as f2:
#     for line in f1:
#         f2.write(f1)

import os
import time

# 修改文件，把源文件写到新的文件，然后把源文件去掉，把新的文件重命名为源文件名
with open('t1_要修改的文件', mode='r', encoding='utf-8') as f1, \
        open('要修改的文件副本', mode='w', encoding='utf-8') as f2:
    for line in f1:
        line=line.strip()  # 去掉换行
        if line.startswith('周'):
            line = line.replace('周', '王')
        f2.write(line)
        f2.write('\n')  # 加回换行
time.sleep(3)
os.remove('t1_要修改的文件')  # 移除源文件
time.sleep(3)
os.rename('要修改的文件副本', 't1_要修改的文件')  # 重命名





