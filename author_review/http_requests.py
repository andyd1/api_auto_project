# _*_ encoding=utf-8 _*_
# @time:2022/2/9 
# Author:赵王发
# @file:http_requests.py
# @software:PyCharm
# 封装requests请求
import requests

# 热词搜索
search_hint_url = 'https://shopee.com.my/api/v4/search/search_hint'
data = 'keyword=mask&search_type=0&version=1'
res = requests.get(search_hint_url, params=data)
print(res.json())
