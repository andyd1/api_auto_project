# _*_ encoding=utf-8 _*_
# @time:2022/1/15 
# Author:赵王发
# @file:t7_global和nonlocal.py
# @software:PyCharm
"""
global 在局部，引入全局变量
nonlocal  # 在局部，向外找一层，看看有没有该变量，如果有就引入，如果没有，继续向外一层，直到全局（不包括全局）
"""


# a = 10
#
#
# def func1():
#     global a
#     a = 20
#
#
# func1()
# print(a)

def func1():
    a = 20

    def fun():
        def func2():
            nonlocal a  # 向外找一层，看看有没有该变量，如果有就引入，如果没有，继续向外一层，直到全局（不包括全局）
            a = 10
        func2()
    fun()
    print(a)


func1()
