# _*_ encoding=utf-8 _*_
# @time:2022/1/15 
# Author:赵王发
# @file:t4_函数编程.py
# @software:PyCharm
"""
递归
函数的嵌套
变量的作用域
闭包
装饰器
    def wrapper(func):
        def inner(*args,**kwargs):
            ret = func(*args,**kwargs)
            return ret
        return inner
    @wrapper
    def func():
        pass
迭代器
生成器
    yield

    g = (x for x in xx)
推导式
匿名函数
python内置函数有下划线"_"、sorted、filter、map
"""

