# _*_ encoding=utf-8 _*_
# @time:2022/1/15 
# Author:赵王发
# @file:t3_内置函数.py
# @software:PyCharm
"""
内置函数：直接调用的函数
print
input
"""
# bin、oct、hex
# a = 20
# print(bin(a))  # 二进制
# print(oct(a))  # 八进制
# print(hex(a))  # 十六进制
#
# # sum、min、max、pow
# b = 10
# c = 3
# print(pow(b, c))  # 10的3次方
# print(b**c)  # 10的3次方
#
# # list
# s = {1,2,3,3}
# lis = list(s)
# print(lis)

# format、ord、chr
a = 18
print(format(a, 'b'))  # b:二进制；o:八进制；x:十六进制
print(format(a, '08b'))  # b:二进制；o:八进制；x:十六进制

s = '中'
print(ord(s))  # ”中“字在Unicode中马位是20013
print(chr(20013))  # 给出编码位置上的文字

# enumerate、all、any
print(all([0,'','word']))  # 当成and来看
print(any([0,'']))  # 当成or来看
lis1 = ['hi', 'word', 'kiki']
for index, item in enumerate(lis1):
    print(index, item)

s1 = '哈哈哈'
print(hash(s))  # 转换后的数据，然后存储
print(id(s))  # 真正存在的内存地址

print(dir(s))  # 当前数据可以执行哪些操作

